import java.util.Stack;
import java.lang.Math;

public class Postfix
{
    private String expr;
    
    public Postfix(String e)
    {
        expr = e;
    }
    
    public double eval()
    {
        String[] tokens = expr.split(" ");
        Stack<Double> s = new Stack<Double>();
        
        for (String token : tokens)
        {
            double x, y;
            switch(token)
            {
                case "+":
                    x = s.pop();
                    y = s.pop();
                    s.push(x+y);
                    break;
                case "-":
                    x = s.pop();
                    y = s.pop();
                    s.push(y-x);
                    break;
                case "*":
                    x = s.pop();
                    y = s.pop();
                    s.push(x*y);
                    break;
                case "/":
                    x = s.pop();
                    y = s.pop();
                    s.push(y/x);
                    break;
                case "^":
                    x = s.pop();
                    y = s.pop();
                    s.push(Math.pow(y,x));
                    break;
                case "sqrt":
                    x = s.pop();
                    s.push(Math.sqrt(x));
                    break;
                case "sin":
                    x = s.pop();
                    s.push(Math.sin(Math.toRadians(x)));
                    //s.push(Math.toRadians(Math.sin(x)));
                    break;
                case "cos":
                    x = s.pop();
                    s.push(Math.cos(Math.toRadians(x)));
                    break;
                case "tan":
                    x = s.pop();
                    s.push(Math.tan(Math.toRadians(x)));
                    break;
                case "cot":
                    x = s.pop();
                    s.push(s.push(Math.cos(Math.toRadians(x)))/s.push(Math.sin(Math.toRadians(x))));
                    break;
                case "log":
                    x = s.pop();
                    s.push(Math.log10(x));
                    break;
                    
                default:
                    s.push(Double.parseDouble(token));
                    break;  
            }
        }
        return s.pop();
    }
}