/*import java.util.Stack;
import java.util.ArrayList;

public class SY
{
    private String infix;
    
    public SY(String expr)
    {
        infix = expr;
        
    }
 
    private int p(String op)
    {
        switch(op)
        {
            case "sin":
            case "cos":
            case "tan":
            case "cot":
            case "log":
            case "sqrt":
                return 4;
            case "^":
                return 3;
            case "*":
            case "/":
                return 2;
            case "+":
            case "-":
                return 1;
            case "z":
            case "l":
            case "v":
            case "h":
            case "k":
            case "u":
                return 5;
            case "(":
            case ")":
                return 6;
            default:
                return 0;
        }
    }
    
    public String toPostfix()
    {
        infix = infix.replace(" ", "");
        infix = infix.replace("sqrt", "z");
        infix = infix.replace("log", "l");
        infix = infix.replace("cot", "v");
        infix = infix.replace("tan", "h");
        infix = infix.replace("cos", "k");
        infix = infix.replace("sin", "u");

        String s = "" + infix.charAt(0);
        for(int i=0; i<infix.length()-1; i++)
        {
            if(p("" + infix.charAt(i))==0 && p("" + infix.charAt(i+1))==0)
            {
                s = s + infix.charAt(i+1);
            }
            else
            {
                s = s + " " + infix.charAt(i+1);
            }
            
        }
        
        s = s.replace("z", "sqrt");
        s = s.replace("l", "log");
        s = s.replace("v", "cot");
        s = s.replace("h", "tan");
        s = s.replace("k", "cos");
        s = s.replace("u", "sin");
        infix = s;
        //infix = infix.replace("", " ").trim();
        // A stack of strings for the operators
        Stack<String> stack = new Stack<String>();
        
        // A list of strings for output
        ArrayList<String> postfix = new ArrayList<String>();
        
        // An array of Strings for tokens
        String[] tokens;
        
        // Split infix expression into tokens
        tokens = infix.split(" ");
        
        // Loop through tokens
        for (String token : tokens)
        {
            
            if (p(token) > 0)  // It's an op!
            {
                if(stack.empty())
                {
                    stack.push(token);
                }
                else
                {
                    if(p(token) < p(stack.peek()) || p(token) == p(stack.peek()))
                    {
                        postfix.add(stack.pop());
                        stack.push(token);
                    }
                    else
                    {
                        stack.push(token);
                    }
                }
                
            }
            //else if (token.equals("("))
            //{
             //   postfix.add(token);
            //}
            
            else if (token.equals(")"))
            {
                while(!stack.peek().equals("("))
                {
                    postfix.add(stack.pop());
                }
                stack.pop();
            }
            //else if(token.equals("x"))
            //{
            //    postfix.add(token);
            //}
            else       // It's (probably) a number!
            {
                postfix.add(token);
            }
        }
        
        // empty out the stack
        while (! stack.empty())
        {
            postfix.add(stack.pop());
        }
        
        // Return a string with space as delimiter
        //System.out.println(String.join(" ", postfix));
        SY2 dumb = new SY2(String.join(" ", postfix));
        return dumb.toPostfix();
    }
}*/

import java.util.Stack;
import java.util.ArrayList;

public class SY
{
    private String infix;
    
    public SY(String expr)
    {
        infix = expr;
    }
 
    private int p(String op)
    {
        switch(op)
        {
            case "^":
            case "sqrt":
            case "sin":
            case "cos":
            case "tan":
            case "cot":
            case "log":
                return 3;
            case "*":
            case "/":
                return 2;
            case "+":
            case "-":
                return 1;
            default:
                return 0;
        }
    }
    
    public String toPostfix()
    {
        infix = infix.replace(" ", "");
        infix = infix.replace("sqrt", "z");
        infix = infix.replace("log", "l");
        infix = infix.replace("cot", "v");
        infix = infix.replace("tan", "h");
        infix = infix.replace("cos", "k");
        infix = infix.replace("sin", "u");
        String s = "" + infix.charAt(0);
        for(int i=0; i<infix.length()-1; i++)
        {
            if(infix.charAt(i) == 'z' || infix.charAt(i) == 'l' || 
                infix.charAt(i) == 'v' || infix.charAt(i) == 'h' || 
                infix.charAt(i) == 'k' || infix.charAt(i) == 'u')
            {
                s = s + " " + infix.charAt(i+1);
            }
            else if(infix.charAt(i) == ('(') || infix.charAt(i) == (')'))
            {
                s = s + " " + infix.charAt(i+1);
            }
            else if(p("" + infix.charAt(i))==0 && p("" + infix.charAt(i+1))==0 
                && infix.charAt(i+1) != ('(') && infix.charAt(i+1) != (')')
                && infix.charAt(i) != ('(') && infix.charAt(i) != (')'))
            {
                s = s + infix.charAt(i+1);
            }
            else
            {
                s = s + " " + infix.charAt(i+1);
            }
        }
        s = s.replace("z", "sqrt");
        s = s.replace("l", "log");
        s = s.replace("v", "cot");
        s = s.replace("h", "tan");
        s = s.replace("k", "cos");
        s = s.replace("u", "sin");
        infix = s;
        // A stack of strings for the operators
        Stack<String> stack = new Stack<String>();
        
        // A list of strings for output
        ArrayList<String> postfix = new ArrayList<String>();
        
        // An array of Strings for tokens
        String[] tokens;
        
        // Split infix expression into tokens
        tokens = infix.split(" ");
        
        // Loop through tokens
        for (String token : tokens)
        {
            
            if (p(token) > 0)  // It's an op!
            {
                if(stack.empty())
                {
                    stack.push(token);
                }
                else
                {
                    if(p(token) < p(stack.peek()) || p(token) == p(stack.peek()))
                    {
                        postfix.add(stack.pop());
                        stack.push(token);
                    }
                    else
                    {
                        stack.push(token);
                    }
                }
                
            }
            else if (token.equals("("))
            {
                stack.push(token);
            }
            else if (token.equals(")"))
            {
                while(!stack.peek().equals("("))
                {
                    postfix.add(stack.pop());
                }
                stack.pop();
            }
            else if(token.equals("x"))
            {
                postfix.add(token);
            }
            else        // It's (probably) a number!
            {
                postfix.add(token);
            }
        }
        
        // empty out the stack
        while (! stack.empty())
        {
            postfix.add(stack.pop());
        }
        
        // Return a string with space as delimiter
        //System.out.println(String.join(" ", postfix));
        return String.join(" ", postfix);
    }
}