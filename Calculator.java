import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.media.AudioClip;
import javafx.scene.shape.Line;
import javafx.scene.shape.Circle;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.lang.StringIndexOutOfBoundsException;

public class Calculator extends Application
{
    private TextField infixField;
    private TextField postfixField;
    private TextField resultField;
    private Label errorLabel;
    ImageView spongebob;
    AudioClip Button;
    AudioClip Switch;
    AudioClip Woosh;
    double ans;

    
    public void start(Stage window)
    {
        window.setTitle("Calculator");
        window.show();
        
        Pane canvas = new Pane();
        Scene scene = new Scene(canvas, 300, 400);
        window.setScene(scene);
        spongebob = new ImageView("spongebob.jpg");
        spongebob.setFitHeight(500);
        spongebob.setFitWidth(300);
        canvas.getChildren().add(spongebob);
        
        //download audiofile
        Button = new AudioClip("file:Button.wav");
        Switch = new AudioClip("file:Switch.wav");
        Woosh = new AudioClip("file:Woosh.wav");
        //Line line = new Line9100, 10, 10, 110);
        //Line line = new Line();
        //Line.setStartX(100);
        //Line.setStartY(10);
        //Line.setEndX(10);
        //Line.setEndY(100);
        
        
        Label infixLabel = new Label("Infix");
        Label postfixLabel = new Label("Postfix");
        Label resultLabel = new Label("Result");
        errorLabel = new Label("Invalid Input");
        errorLabel.setVisible(false);
        errorLabel.setTextFill(Color.BLACK);
        errorLabel.setFont(Font.font("Times New Roman", 24));
        
        infixField = new TextField();
        infixField.setPromptText("Enter Calculation");
        infixField.setPrefWidth(165);
        infixField.positionCaret(0);
        postfixField = new TextField();
        postfixField.setPrefWidth(165);
        postfixField.setPromptText("Postfixed Format");
        resultField = new TextField();
        resultField.setPrefWidth(165);
        resultField.setPromptText("Result appears here");
        
        Button goButton = new Button("Enter");
        goButton.setStyle("-fx-background-color: #59ff82; ");
        Button clearButton = new Button("Clear");
        clearButton.setStyle("-fx-background-color: #ff4300; ");
        Button zeroButton = new Button("0");
        zeroButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button oneButton = new Button("1");
        oneButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button twoButton = new Button("2");
        twoButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button threeButton = new Button("3");
        threeButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button fourButton = new Button("4");
        fourButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button fiveButton = new Button("5");
        fiveButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button sixButton = new Button("6");
        sixButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button sevenButton = new Button("7");
        sevenButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button eightButton = new Button("8");
        eightButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button nineButton = new Button("9");
        nineButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button plusButton = new Button("+");
        plusButton.setStyle("-fx-background-color: #21adff; ");
        Button minusButton = new Button("-");
        minusButton.setStyle("-fx-background-color: #21adff; ");
        Button divideButton = new Button("/");
        divideButton.setStyle("-fx-background-color: #21adff; ");
        Button openParaButton = new Button("(");
        openParaButton.setStyle("-fx-background-color: #21adff; ");
        Button closeParaButton = new Button(")");
        closeParaButton.setStyle("-fx-background-color: #21adff; ");
        Button powButton = new Button("^");
        powButton.setStyle("-fx-background-color: #21adff; ");
        Button multiButton = new Button("*");
        multiButton.setStyle("-fx-background-color: #21adff; ");
        Button sqrtButton = new Button("√");
        sqrtButton.setStyle("-fx-background-color: #21adff; ");
        Button backSpaceButton = new Button("Back");
        backSpaceButton.setStyle("-fx-background-color: #ffce49; ");
        Button ansButton = new Button("Ans");
        ansButton.setStyle("-fx-background-color: #f9e8d6; ");
        Button graphButton = new Button("Graph");
        graphButton.setStyle("-fx-background-color: #00ddff; ");
        Button sinButton = new Button("Sin");
        sinButton.setStyle("-fx-background-color: #ff5151; ");
        Button cosButton = new Button("Cos");
        cosButton.setStyle("-fx-background-color: #ff5151; ");
        Button tanButton = new Button("Tan");
        tanButton.setStyle("-fx-background-color: #ff5151; ");
        Button cotButton = new Button("Cot");
        cotButton.setStyle("-fx-background-color: #ff5151; ");
        Button logButton = new Button("Log");
        logButton.setStyle("-fx-background-color: #ff5151; ");
        Button xButton = new Button("X");
        xButton.setStyle("-fx-background-color: #f9e8d6; ");
        //logButton.setMinHeight(25);
        //logButton.setStyle("-fx-font-size:1");
        //logButton.setFont(new Font(26));
        //graphButton.setPrefWidth(1);
        //logButton.setFont(Font.font("Times New Roman", 10));
        
      
        infixLabel.relocate(20, 25);
        postfixLabel.relocate(20, 62);
        infixField.relocate(65, 20);
        postfixField.relocate(65, 60);
        
        xButton.relocate(205, 150);
        xButton.setPrefWidth(25);
        goButton.relocate(100, 290);
        goButton.setPrefWidth(60);
        clearButton.relocate(188, 273);
        clearButton.setPrefWidth(60);
        clearButton.setRotate(-90);
        errorLabel.relocate(85, 120);
        resultLabel.relocate(20, 100);
        resultField.relocate(65, 95);
        zeroButton.relocate(65, 290);
        zeroButton.setPrefWidth(25);
        oneButton.relocate(65, 255);
        oneButton.setPrefWidth(25);
        twoButton.relocate(100, 255);
        twoButton.setPrefWidth(25);
        threeButton.relocate(135, 255);
        threeButton.setPrefWidth(25);
        fourButton.relocate(65, 220);
        fourButton.setPrefWidth(25);
        fiveButton.relocate(100, 220);
        fiveButton.setPrefWidth(25);
        sixButton.relocate(135, 220);
        sixButton.setPrefWidth(25);
        sevenButton.relocate(65, 185);
        sevenButton.setPrefWidth(25);
        eightButton.relocate(100, 185);
        eightButton.setPrefWidth(25);
        nineButton.relocate(135, 185);
        nineButton.setPrefWidth(25);
        plusButton.relocate(170, 185);
        plusButton.setPrefWidth(25);
        minusButton.relocate(170, 220);
        minusButton.setPrefWidth(25);
        divideButton.relocate(170, 255);
        divideButton.setPrefWidth(25);
        multiButton.relocate(170, 290);
        multiButton.setPrefWidth(25);
        openParaButton.relocate(135, 150);
        openParaButton.setPrefWidth(25);
        closeParaButton.relocate(170, 150);
        closeParaButton.setPrefWidth(25);
        powButton.relocate(65, 150);
        powButton.setPrefWidth(25);
        sqrtButton.relocate(100, 150);
        sqrtButton.setPrefWidth(25);
        backSpaceButton.relocate(188, 202);
        backSpaceButton.setPrefWidth(60);
        backSpaceButton.setRotate(-90);
        ansButton.relocate(155, 360);
        ansButton.setPrefWidth(40);
        graphButton.relocate(188, 343);
        graphButton.setRotate(-90);
        //graphButton.setPrefWidth(25);
        graphButton.setPrefWidth(60);
        sinButton.relocate(65, 360);
        sinButton.setPrefWidth(40);
        cosButton.relocate(110, 325);
        cosButton.setPrefWidth(40);
        tanButton.relocate(110, 360);
        tanButton.setPrefWidth(40);
        cotButton.relocate(155, 325);
        cotButton.setPrefWidth(40);
        logButton.relocate(65, 325);
        logButton.setPrefWidth(40);
        
        canvas.getChildren().addAll(infixLabel, postfixLabel, 
            infixField, postfixField, goButton, clearButton
            ,errorLabel, resultField, resultLabel, zeroButton, 
            oneButton, twoButton, threeButton, fourButton, 
            fiveButton, sixButton, sevenButton, eightButton, 
            nineButton, plusButton, minusButton, divideButton,
            multiButton, openParaButton, closeParaButton,
            powButton, sqrtButton, backSpaceButton, ansButton, 
            graphButton, sinButton, cosButton, tanButton, cotButton, 
            logButton, xButton);
        
        goButton.setOnAction(this::handleGoButton);
        clearButton.setOnAction(this::handleClearButton);
        zeroButton.setOnAction(this::handleZeroButton);
        oneButton.setOnAction(this::handleOneButton);
        twoButton.setOnAction(this::handleTwoButton);
        threeButton.setOnAction(this::handleThreeButton);
        fourButton.setOnAction(this::handleFourButton);
        fiveButton.setOnAction(this::handleFiveButton);
        sixButton.setOnAction(this::handleSixButton);
        sevenButton.setOnAction(this::handleSevenButton);
        eightButton.setOnAction(this::handleEightButton);
        nineButton.setOnAction(this::handleNineButton);
        plusButton.setOnAction(this::handlePlusButton);
        minusButton.setOnAction(this::handleMinusButton);
        divideButton.setOnAction(this::handleDivideButton);
        multiButton.setOnAction(this::handleMultiButton);
        powButton.setOnAction(this::handlePowButton);
        openParaButton.setOnAction(this::handleOpenParaButton);
        closeParaButton.setOnAction(this::handleCloseParaButton);
        sqrtButton.setOnAction(this::handleSqrtButton);
        backSpaceButton.setOnAction(this::handleBackSpaceButton);
        ansButton.setOnAction(this::handleAnsButton);
        graphButton.setOnAction(this::handleGraphButton);
        sinButton.setOnAction(this::handleSinButton);
        cosButton.setOnAction(this::handleCosButton);
        tanButton.setOnAction(this::handleTanButton);
        cotButton.setOnAction(this::handleCotButton);
        logButton.setOnAction(this::handleLogButton);
        xButton.setOnAction(this::handleXButton);
        
        window.setResizable(false);
        window.sizeToScene();
        window.getIcons().add(new Image("file:calc-icon.PNG"));
    }
    
    public void handleGoButton(ActionEvent e)
    {
        
        //get value from feetField
        String input = infixField.getText();
        //double ans;
        //create a feet object
        try
        {
            SY s = new SY(input);
            String p = s.toPostfix();
            //convert to smoots
            //double postfix = s.p();
            //update smootField
            postfixField.setText(p);
            Postfix m = new Postfix(p);
            
            resultField.setText("" + m.eval());
            ans = m.eval();
            
            //plays audio
            Switch.play(.15);
            
            
            //make error invisible
            errorLabel.setVisible(false);
        }
        catch (NumberFormatException | EmptyStackException | StringIndexOutOfBoundsException nfe)
        {
            errorLabel.setVisible(true);
        }
    }
    
    public void handleClearButton(ActionEvent e)
    {
        postfixField.setText("");
        infixField.setText("");
        errorLabel.setVisible(false);
        Woosh.play(.15);
    }
    
    public void handleZeroButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "0");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleOneButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "1");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleTwoButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "2");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleThreeButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "3");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleFourButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "4");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleFiveButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "5");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleSixButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "6");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleSevenButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "7");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleEightButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "8");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleNineButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "9");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handlePlusButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "+");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleMinusButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "-");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleDivideButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "/");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleMultiButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "*");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handlePowButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "^");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleOpenParaButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "(");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleCloseParaButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + ")");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleSqrtButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "sqrt");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleSinButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "sin");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleCosButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "cos");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleTanButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "tan");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleCotButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "cot");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleLogButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "log");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleXButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + "x");
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleBackSpaceButton(ActionEvent e)
    {
        infixField.end();
        infixField.deletePreviousChar();
        errorLabel.setVisible(false);
        Woosh.play(.15);
    }
    
    //implement this in SY
    /*
    public String isEmpty()
    {
        if (infixField.getText().isEmpty())
        return "";
        else 
        return " ";
    }*/
    
    public void handleAnsButton(ActionEvent e)
    {
        infixField.setText(infixField.getText() + ans);
        errorLabel.setVisible(false);
        Button.play(.15);
    }
    
    public void handleGraphButton(ActionEvent e)
    {
        //Circle circles[] = new Circle[10];
        Switch.play(.15);
        String input = infixField.getText();
        SY s = new SY(input);
        String shunted = s.toPostfix();
        postfixField.setText(shunted);
        Function expr = new Function(shunted);
        resultField.setText("y = " + infixField.getText());
        //resultField.setText("" + expr.yVal(10));
        
        
        /*
        Line segOne = new Line(200, 200 + expr.yVal(0), 210, 200 + expr.yVal(10));
        segOne.setStroke(Color.RED);
        Line segTwo = new Line(210, 200 + expr.yVal(10), 220, 200 + expr.yVal(20));
        segTwo.setStroke(Color.BLUE);
        */
        
        
        Pane secondaryLayout = new Pane();
        Scene graphScene = new Scene(secondaryLayout, 400, 400);
        Stage newWindow = new Stage();
        newWindow.setTitle("Graph");
        newWindow.setScene(graphScene);
        newWindow.show();
        newWindow.setResizable(false);
        newWindow.sizeToScene();
        newWindow.getIcons().add(new Image("file:graph-icon.PNG"));
        
        
        Line xAxis = new Line(0, 200, 400,200);
        Line yAxis = new Line(200, 0, 200, 400);
        //yAxis.setStroke(Color.RED);
        //secondaryLayout.getChildren().addAll(dot1, dot2, dot3, dot4, dot5);
        secondaryLayout.getChildren().addAll(xAxis, yAxis);
        //secondaryLayout.getChildren().add(lines[]);
        
        //Circle[] dots = new Circle[50];
        
        for(double i=0; i<10000; i=i+1)
        {
            Circle dot = new Circle(200 + i/100, 200 - expr.yVal(i/100), 0.5);
            secondaryLayout.getChildren().add(dot);
        }
        
        for(double i=0; i<10000; i=i+1)
        {
            Circle dot = new Circle(200 + -i/100, 200 - expr.yVal(-i/100), 0.5);
            secondaryLayout.getChildren().add(dot);
        }
        
        /*
        for(int i=0; i<1000; i++)
        {
            Line line = new Line();
            line.setStartX(200 + i);
            line.setStartY(200 - expr.yVal(i));
            line.setEndX(200 + i);
            line.setEndY(200 - expr.yVal(i));
            secondaryLayout.getChildren().add(line);
        }
        
        for(int i=0; i<1000; i++)
        {
            Line line = new Line();
            line.setStartX(200 - i);
            line.setStartY(200 + expr.yVal(i));
            line.setEndX(200 - i);
            line.setEndY(200 + expr.yVal(i));
            secondaryLayout.getChildren().add(line);
        }
        */
    }
    
}