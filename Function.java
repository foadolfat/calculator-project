import java.util.ArrayList;

public class Function
{
    private String expr;
    
    public Function(String expr)
    {
        this.expr=expr;
    }
    
    public int p(String op)
    {
        switch(op)
        {
            case "x":
                return 1;
            default:
                return 0;
        }
    }
    
    public double yVal(double x)
    {
        /*String[] tokens = expr.split(" ");
        String xVal = "" + x;
        for (String token : tokens)
        {
            if(p(token)==1)
            {
                token = xVal;
            }
        }*/
        String xVal = "" + x;
        String newValue = expr.replaceAll("x", xVal);
        Postfix yVal = new Postfix(newValue);
        return yVal.eval();//String.join(" ", tokens);//yVal.eval();
    }
    
    
}